var CellSelected_x;
var CellSelected_y;
var Moves;
var Options;

var secs;
var mins;
var cronometer;

var board = new Array(8);


//Reinicia el cronometro de tiempo que se visualiza en pagina web 
function ResetTime(){
    clearInterval(cronometer);
}
//inicia el cronometro de tiempo que se visualiza en pagina web 
function StarTime(){
    secs =0;
    mins =0;
    //accede a los elemtos html que muestran las vaiables de tiempo( minutos y segundos)
    s = document.getElementById("seconds");
    m = document.getElementById("minutes");

    cronometer = setInterval(
        function(){
            if(secs==60){
                secs=0;
                mins++;
                if (mins<10) m.innerHTML ="0"+mins;
                else m.innerHTML = mins;

                if(mins==60) mins=0;
            }
            if (secs<10) s.innerHTML ="0"+secs;
            else s.innerHTML = secs;

            secs++;
        }
        ,1000);
}

//Solo indica si a finalizado o no?
//Esta funcion se apoya de la funcion que tiene la logia 
//del juego que exmina si tengo mas movimientos disponibles
function Check_SuccessfulEnd(){

	SuccessfulEnd = true;
	if (Moves > 0) SuccessfulEnd = false;
	
	if (SuccessfulEnd){
		ShowGameOver("Ganaste!!", "Vuelva a Jugar", false);
		autoplay();
	} 
}

//Notifica que se pierde la partida, asi que muestra el mensaje de fracaso.
function Check_GameOver(x, y){

	Options = 0;

	Check_Move(x, y, 1, 2);		//chequea e movimiento de la Dereha a lo largo de arriba
	Check_Move(x, y, 2, 1);		//chequea e movimiento de lo largo de la Dereha hacia arriba
	Check_Move(x, y, 1, -2);	//chequea e movimiento de Dereha  a lo largo hacia abajo
	Check_Move(x, y, 2, -1);	//chequea e movimiento de lo largo de la Dereha  hacia abajo
	Check_Move(x, y, -1, 2);	//chequea e movimiento de iquierda a  lo largo de arriba
	Check_Move(x, y, -2, 1);	//chequea e movimiento de lo largo de la iquierda hacia arriba
	Check_Move(x, y, -1, -2);	//chequea e movimiento de iquierda a lo largo hacia abajo
	Check_Move(x, y, -2, -1);	//chequea e movimiento de lo largo de la iquierda hacia abajo

	cont_moves = document.getElementById("options").innerHTML = Options;

	if (!Options){
		//Mensaje cuando superas el tiempo y pierdes
		ShowGameOver("Game Over", "Intentar de Nuevo!", true);
		autoplay();
	}

}

//Aca pintamos la celda, cambiando el fondo de la casilla donde esta la imagen del caballo
function PaintCell(x, y){
	cell = document.getElementById("c"+CellSelected_x+CellSelected_y);
	cell.style.background = "none repeat scroll 0% 0% orange";
	
}
//Marcamos la filla seleccionada,e inertamos la imagen en la casilla que se ha cliqueado.
function SelectCell(x, y){

	Moves--;
	cont_moves = document.getElementById("moves").innerHTML = Moves;
	
	PaintCell(x, y);

	cell = document.getElementById("c"+x+y);
	cell.style.background = "none repeat scroll 0% 0% green";
	cell = document.getElementById("c"+x+y).innerHTML = 
		"<img id='" + x + y + "' src='horse.gif'></img>"
	
	board[x][y]=1; //matriz del tablero 8x8
	CellSelected_x=x;
	CellSelected_y=y;


	Check_SuccessfulEnd(); //Examinamos si con la jugada, a finalizado 
	Check_GameOver(x, y);  // o con esta jugada ha perdido.

}

function CheckCell(x, y){
	dif_x = x - CellSelected_x;
	dif_y = y - CellSelected_y;
	CheckTrue = false;

	if (dif_x == 1 && dif_y == 2)   CheckTrue = true; // Dereha a lo largo de arriba
	if (dif_x == 1 && dif_y == -2)  CheckTrue = true; // lo largo de la Dereha hacia arriba
	if (dif_x == 2 && dif_y == 1)   CheckTrue = true; // Dereha  a lo largo hacia abajo
	if (dif_x == 2 && dif_y == -1)  CheckTrue = true; // lo largo de la Dereha  hacia abajo
	if (dif_x == -1 && dif_y == 2)  CheckTrue = true; // iquierda a  lo largo de arriba
	if (dif_x == -1 && dif_y == -2) CheckTrue = true; // lo largo de la iquierda hacia arriba
	if (dif_x == -2 && dif_y == 1)  CheckTrue = true; // iquierda a lo largo hacia abajo
	if (dif_x == -2 && dif_y == -1) CheckTrue = true; // lo largo de la iquierda hacia abajo


	if (board[x][y]==1) CheckTrue=false;

	if (CheckTrue) SelectCell(x, y);
}

//Con esta funcion reiniciamos todo el Juego, Las jugadas del tablero, reinicia el tiempo o cronometro.
function autoplay(){
	
	message = document.getElementById("message");
	message.style.display = "none";

	for (i=0; i<10; i++) board[i]= new Array(8);

	ClearBoard();
	ResetTime();
	StarTime();
	Moves=64;
	
	
	//Possion del primer Caballo, Aleatorio
	x=Math.round(Math.random()*7);
	y=Math.round(Math.random()*7);
	
	CellSelected_x=x;
	CellSelected_y=y;

	SelectCell(x, y);
}


autoplay();

//Controla el mensaje de Perdida o triunfo
//Para que no se muestre el trozo de html unicamente cuando sea necesario y no por ejemplo al inicar el juego, 
function hide_message(){
	message = document.getElementById("message");
	message.style.display = "none";
	autoplay();
}

function ClearBoard(){
	for (i=0; i<8; i++){
		for (j=0; j<8; j++){
			board[i][j]=0;

			cell = document.getElementById("c"+i+j);
			cell.style.background = "";  
			cell = document.getElementById("c"+i+j).innerHTML = "";
		}
	}

}
//Contruye todo lo que se observa al finalizar el juego, que es la cinta de color negro con 
//el respectivo mensaje.
function ShowGameOver(string_notification, string_button, game_over){
	ResetTime();
		score_min = mins;
		score_sec = secs;
		string_score="";
		if (score_min<10) string_score="0";
		string_score=string_score + score_min + ":";
		if (score_sec<10) string_score=string_score +"0";
		string_score=string_score + score_sec;

		

		//funcion para el despliegue de la información final del Juego
			GameOver = document.getElementById("message");
			GameOver.style.display = "block";
			Message_GameOver=document.getElementById("notification");
			Message_GameOver=document.getElementById("notification").innerHTML=string_notification;
			Message_GameOver=document.getElementById("final_score");
			Message_GameOver=document.getElementById("final_score").innerHTML="Time " + string_score;
			Message_GameOver=document.getElementById("button");
			Message_GameOver=document.getElementById("button").innerHTML= string_button;
			
			if (game_over == true){
				Tweet_Game=document.getElementById("tweet_game_ok");
				Tweet_Game.style.display = "none";
			}
			else{
				Tweet_Game=document.getElementById("tweet_game_over");
				Tweet_Game.style.display = "none";
			}
		WhiteRestart();
}
//Vemos si la posicion del tableo seleccionada esta libre.
function Check_Move(x, y, mov_x, mov_y){
		option_x = x+mov_x;
		option_y = y+mov_y;

		if (option_x < 8 && option_y < 8 && 
			option_x >= 0 && option_y >= 0){

		if (board[option_x][option_y] == 0) Options++;

	}
}
